<?php 
#01. echo vs print

Answer: We can use echo And  print statements to output variables or strings.

echo:
echo is a statement, which is used to display the output.
echo can be used with or without parentheses.
echo does not return any value.
We can pass multiple strings separated by comma (,) in echo.
echo is faster than print statement.

example: echo "This ", "string ", "was ", "made ", "with multiple parameters.";

print:
print is also a statement, used as an alternative to echo at many times to display the output.
print can be used with or without parentheses.
print always returns an integer value, which is 1.
Using print, we cannot pass multiple arguments.
print is slower than echo statement.

example: print "Hello world!<br>";

echo vs print
        a.echo has no return value while print has a return value of 1 so it can be used in expressions. 
    `   b.echo can take multiple parameters (although such usage is rare) while print can take one argument. 
        c.echo is marginally faster than print 

#02. single quote vs double quote php

The most significant difference between the single and the double quotes lies when we interpolate the string and the variable. 

Single quote:
The single quote does not interpolate the string and the variables. The content inside the single quote prints out as exactly as it is. In most cases, there is no compilation of any variables or escape sequences inside the single quote.

$name = "Mustafa";
echo 'The name is $name.'; 
echo 'Mustafa Ahmed\'s  \"Think and grow rich\".';

Output:
The name is $name.
Mustafa Ahmed's \"Think and grow rich\".

Double quote:
In the case of the double quote, the variable written inside the quotes will be interpolated with the string. It means that the variable in the string will be evaluated. Consequently, it is easy to use double quotes when interpolating the string and the variables.


$name = "Mustafa";
echo "The name is $name. \n";
echo "Mustafa Ahmed's \"Think and grow rich\". \n";

Output:
The name is Mustafa.
Mustafa Ahmed's  "Think and grow rich".

#03. heredoc vs nowdoc

Heredoc strings are like double-quoted strings without escaping. 
Begin a string with <<< and an identifier, then put the string in a
new line. Close it in another line by repeating the identifier.
heredoc behaves like double-quoted strings.

//heredoc example
$name = 'Todd Eidson';
$text = <<<txt
My name is $name
txt

echo $text;
// outputs 'My name is Todd Eidson'

Nowdoc strings are like single-quoted strings without escaping.
Is what heredoc is for double-quoted strings but for single quotes. It
works the same way and eliminates the need for escape characters.


//nowdoc example
$name = 'Todd Eidson';  // notice the single quotes around the marker 
$text = <<<'txt'
My name is $name
txt

echo $text;
// outputs 'My name is $name'


#04. function, array function, string function example



